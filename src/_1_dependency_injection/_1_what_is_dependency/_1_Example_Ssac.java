package _1_dependency_injection._1_what_is_dependency;


class TeacherJSP {
  public void doSmallTalk() {
    System.out.println("Small talk....");
  }

  public void doTeachJSP() {
    System.out.println("JSP");
  }

  public String changeCurriculum() {
    return "JSP";
  }

}

class _Ssac {
  private final int CLASS_TIME = 50;
  private String curriculum;
  TeacherJSP teacher = new TeacherJSP();

  public void doClass(String question) {
    for (int i = 0; i < CLASS_TIME * 0.8; i++) {
      teacher.doSmallTalk();
    }

    if (!question.isEmpty()) {
      this.curriculum = teacher.changeCurriculum();
    }

    teacher.doTeachJSP();
  }
}

public class _1_Example_Ssac {
  public static void main(String[] args) {
    _Ssac ssac = new _Ssac();

    String question = "Question";
    ssac.doClass(question);
  }
}
